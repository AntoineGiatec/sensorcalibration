SensorCalibration App

Instructions:
It is recommended to use the app in the following steps:

Setting up the files: Ensure that the CSV are withing a certain tolerance of each other for their first points as they
are assumed to be 15 minutes regular intervals from then on as points 1 will be compared, as will points 2, 3, 4, etc...
(otherwise make edits to the files)

1: Select RTD measurements
By pressing on the RTD measurement prompt, you will be asked to locate the RTD measurement

2: Select sensors
The app allows for multiple sensor measurement files to be selected however the first sensor selected will be used as
a reference to synchronize the others.

3. Filter
If the files are set up correctly, the system should complete the compilation and filtering of the data and open the
results in Excel for the user to analyze. The actual file is stored in the ./data/ folder of the calibration app.

Additional Info:
- The program will only accept csv files
- If any of the files selected are undesired, simply reset and re-select the desired files