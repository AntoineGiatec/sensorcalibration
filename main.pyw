#! /usr/bin/python3
# -*- coding:utf-8 -*-

# Calibration App for filtering and testing data:
import os
import datetime as dt
import tkinter as tk
from tkinter import filedialog
from shutil import copyfile


# Constants for future reference
SENSOR_INTERVAL = 15
CALIBRATION_FILE = "calibration.csv"
SMARTROCK3 = True
DIRECTORY_PATH = None


# Functions here
# Select the RTD measurement file and store in rtd_path/rtd_var
def rtd_press():
    global DIRECTORY_PATH
    if DIRECTORY_PATH is not None:
        filename = filedialog.askopenfile(initialdir=DIRECTORY_PATH, title="Select RTD measurements")
    else:
        filename = filedialog.askopenfile(initialdir='/', title="Select RTD measurements")
    if filename is not None:
        if ".csv" in filename.name or ".CSV" in filename.name:
            rtd_path.set(filename.name)
            rtd_var.set(filename.name.split("/")[-1])
            DIRECTORY_PATH = "/" + "/".join(filename.name.split("/")[:-1])
        else:
            rtd_var.set("Invalid entry (csv only)")


# Add a sensor file to the list of sensors to compare
def add_sensor():
    global DIRECTORY_PATH
    if DIRECTORY_PATH is not None:
        filename = filedialog.askopenfile(initialdir=DIRECTORY_PATH, title="Select Sensor")
    else:
        filename = filedialog.askopenfile(initialdir='/', title="Select Sensor")
    if filename is not None:
        if filename.name in sensor_list:
            if "Already in list" not in sensor_names.get():
                sensor_names.set(sensor_names.get() + "\nAlready in list")
        elif ".csv" in filename.name or ".CSV" in filename.name:
            sensor_list.append(filename.name)
            new_label = ""
            for sensor in sensor_list:
                new_label = new_label + ", " + sensor.split("/")[-1][:-4]
            sensor_names.set("Sensors: " + new_label[1:])
            DIRECTORY_PATH = "/" + "/".join(filename.name.split("/")[:-1])


# Reset the program to its initial state
def reset():
    rtd_path.set("")
    rtd_var.set("Please select measurement file")
    sensor_list.clear()
    sensor_names.set("Please select sensor files")
    filter_button.config(fg="black", text="Filter")


# Fetch the files selected by the user
def get_files():
    if ".csv" not in rtd_path.get() and ".CSV" not in rtd_path.get():
        filter_button.config(text="Please select RTD file", fg="red")
        return

    if len(sensor_list) == 0:
        filter_button.config(text="Please select sensor data file", fg="red")
        return

    # Check to see if the data folder exists:
    if not os.path.isdir("./data"):
        os.mkdir("./data")

    # Clean out data folder
    for file in os.listdir("./data"):
        os.remove("./data/" + file)

    # Download temp files to work with
    copyfile(rtd_path.get(), "./data/rtd.csv")
    sensor_files = []
    for sensor in sensor_list:
        copyfile(sensor, "./data/" + sensor.split("/")[-1])
        sensor_files.append("./data/" + sensor.split("/")[-1])

    setup_files(sensor_files, "./data/rtd.csv")
    combine_files("./data/")
    clean_up("./data/" + CALIBRATION_FILE)
    done("./data/" + CALIBRATION_FILE)


# Setup files by using the same date/time format and synchronizing the sensors
def setup_files(item_list, rtd_dir):
    # Setup rtd file to match date time format
    rtd = open(rtd_dir, "r")
    data = []
    for line in rtd.readlines():
        line_date = date_from_string(line.split(",")[0] + " " + line.split(",")[1])
        data.append(line_date.strftime("%m/%d/%Y %H:%M") + "," + line.split(",")[2])
    rtd.close()
    rtd = open(rtd_dir, "w+")
    for line in data:
        rtd.write(line + '\n')
    rtd.close()

    # Open first sensor file and get reference date/time while also setting same format obligations
    file = open(item_list[0], "r")
    reference = None
    data = []
    for line in file.readlines():
        try:
            int(line.split(",")[0])
        except ValueError:
            continue
        if reference is None:
            reference = date_from_string(line.split(",")[1], date_type="MDY")
            line_date = reference
        else:
            line_date = date_from_string(line.split(",")[1], date_type="MDY")

        if SMARTROCK3:
            data.append(line.split(",")[0] + "," + line_date.strftime("%m/%d/%Y %H:%M") +
                        "," + line.split(",")[2] + "," + line.split(",")[3])
        else:
            data.append(line.split(",")[0] + "," + line_date.strftime("%m/%d/%Y %H:%M") +
                        "," + line.split(",")[2])

    file.close()
    file = open(item_list[0], "w+")
    for line in data:
        file.write(line + "\n")
    file.close()
    # Check to see if there is more than file, if there aren't, just return
    if len(item_list) < 2:
        return

    # Go through the rest of the sensors
    data = []
    for file in item_list[1:]:
        sensor = open(file, "r")
        for line in sensor.readlines():
            try:
                int(line.split(",")[0])
            except ValueError:
                continue

            # Setup dates to the same format
            line_date = date_from_string(line.split(",")[1], date_type="MDY")
            if SMARTROCK3:
                data.append(line.split(",")[0] + "," + line_date.strftime("%m/%d/%Y %H:%M") + "," +
                            line.split(",")[2] + "," + line.split(",")[3])
            else:
                data.append(line.split(",")[0] + "," + line_date.strftime("%m/%d/%Y %H:%M") + "," +
                            line.split(",")[2])

        sensor.close()
        sensor = open(file, "w+")
        for line in data:
            sensor.write(line + "\n")
        sensor.close()
        data = []

    # Now every file uses the same time format, it is now possible to compare and see if they have the same timestamps
    for file in item_list[1:]:
        line_date = reference
        if reference.strftime("%m/%d/%Y %H:%M") not in open(file).read():
            sensor = open(file).readlines()
            for line in sensor:
                if SMARTROCK3:
                    data.append(line.split(",")[0] + "," + line_date.strftime("%m/%d/%Y %H:%M") + "," + line.split(",")
                                [2] + "," + line.split(",")[3])
                else:
                    data.append(line.split(",")[0] + "," + line_date.strftime("%m/%d/%Y %H:%M") + "," +
                                line.split(",")[2])
                line_date = line_date + dt.timedelta(seconds=SENSOR_INTERVAL*60)
            new_file = open(file, "w+")
            for line in data:
                new_file.write(line + "\n")
            new_file.close()
            data = []


# Combines all the data into a single file
def combine_files(path):
    # Open the rtd and sensor files
    sensors = []
    rtd = ""
    for file in os.listdir(path):
        if file == "rtd.csv":
            rtd = open(path + file, "r")
        else:
            sensors.append(open(path + file, "r"))

    # Get the needed data from the files:
    rtd_data = rtd.readlines()
    rtd.close()
    sensor_data = []
    for item in sensors:
        sensor_data.append(item.readlines())
        item.close()

    # Initialize the result file
    calibration = open(path + CALIBRATION_FILE, "w+")
    header = "Date Time,Reference Temperature"
    for item in sensors:
        if not SMARTROCK3:
            header = header + "," + item.name.split("/")[-1][:-4]
        else:
            file_name = item.name.split("/")[-1][:-4]
            header = header + "," + file_name + " Cable Temp," + file_name + " Body Temp"
    calibration.write(header + "\n")

    # Start building up the data file
    for line in rtd_data:
        date = line.split(",")[0]
        measurement = line.split(",")[1]

        calibration.write(date + "," + measurement + "\n")
    calibration.close()

    # Get data for sensors
    calibration = open(path + CALIBRATION_FILE, "r")
    file = calibration.readlines()
    calibration.close()

    # Now add sensor data
    for sensor in sensor_data:
        # Trim out the fat of the sensor info
        for line in sensor:
            if not has_numbers(line.split(",")[0]):
                continue
            datetime = line.split(",")[1]

            if not SMARTROCK3:
                file = add_to_line(file, datetime, line.split(",")[2])
            else:
                file = add_to_line(file, datetime, line.split(",")[2] + "," + line.split(",")[3])

    # Write results to calibration file
    calibration = open(path + CALIBRATION_FILE, "w+")
    for line in file:
        calibration.write(line)
    calibration.close()


# Remove the empty space in the calibration file and compile the results
def clean_up(file_path):
    file = open(file_path, "r")
    data = file.readlines()
    file.close()

    file = open(file_path, "w+")
    for line in data:
        if line.count(",") > 1:
            file.write(line)
    file.close()


# Function called upon completion of the script
def done(path=None):
    filter_button.config(text="Completed! File is in the Data folder", fg="green")
    if path is not None:
        file = path.split("/")[-1]
        path = path[:-len(path.split("/")[-1])]
        for item in os.listdir(path):
            if item not in file:
                os.remove(path + "/" + item)
        os.startfile(os.getcwd() + "\\data\\" + file)


# Returns whether there are any numbers in the string
def has_numbers(in_string):
    return any(char.isdigit() for char in in_string)


# Returns a datetime object from a string
# Accepted formats include
#   date_type: DMY -> DD/MM/YY HH:MM
#   date_type: MDY -> MM/DD/YY HH:MM
#   Autodetected -> YYYY-MM-DD hh:mm:ss

def date_from_string(in_string, date_type="DMY"):
    # Assume if / is found that format is DD/MM/YY hh:mm
    if in_string.find('/') >= 0:
        if date_type == "DMY":
            day = int(in_string.split("/")[0])
            mon = int(in_string.split("/")[1])
            year = int(in_string.split("/")[2].split(" ")[0])
            hour = int(in_string.split(" ")[1].split(":")[0])
            mins = int(in_string.split(":")[1])
        else:
            mon = int(in_string.split("/")[0])
            day = int(in_string.split("/")[1])
            year = int(in_string.split("/")[2].split(" ")[0])
            hour = int(in_string.split(" ")[1].split(":")[0])
            mins = int(in_string.split(":")[1])
    # Otherwise it better goddam be YYYY-MM-DD hh:mm:ss
    else:
        year = int(in_string.split("-")[0])
        mon = int(in_string.split("-")[1])
        day = int(in_string.split("-")[2].split(" ")[0])
        hour = int(in_string.split(" ")[1].split(":")[0])
        mins = int(in_string.split(":")[1])

    if year < 2000:
        year = year + 2000

    if "AM" in in_string:
        if hour == 12:
            hour = 0
    elif "PM" in in_string:
        if hour != 12:
            hour = hour + 12

    return dt.datetime(year=year, month=mon, day=day, hour=hour, minute=mins)


# Find an element in a list and add addition to it
def add_to_line(list_elements, element, addition):
    new_list = []
    for item in list_elements:
        if element in item:
            new_list.append(item[:-1] + "," + addition)
        else:
            new_list.append(item)
    return new_list


def change_sensor_type():
    global SMARTROCK3
    if SMARTROCK3:
        SMARTROCK3 = False
        smartrock_type_button.config(text="SmartRock2")
    else:
        SMARTROCK3 = True
        smartrock_type_button.config(text="SmartRock3")


# App will start here
root = tk.Tk()
root.title("Sensor Calibration App")
root.iconbitmap('giatec icon.ico')
root.minsize(900, 600)

root.config(bg="green")
root.grid_rowconfigure([0, 2, 4, 6, 8], weight=1)
root.grid_rowconfigure([1, 3, 5, 7], weight=3)
root.grid_columnconfigure([0, 2, 4], weight=1)
root.grid_columnconfigure(1, weight=3)

rtd_path = tk.StringVar()
rtd_var = tk.StringVar()
rtd_var.set("Please select measurement file")
rtd_label = tk.Label(root, textvariable=rtd_var, bg="green", fg="white", font="calibri 15 bold")
rtd_button = tk.Button(root, text="RTD Sensor Data\nDD/MM/YYYY", command=rtd_press, bg="white", font="calibri 15 bold")
sensor_list = []
sensor_names = tk.StringVar()
sensor_names.set("Please select sensor files")
sensor_label = tk.Label(root, textvariable=sensor_names, bg="green", fg="white", font="calibri 15 bold")
sensor_button = tk.Button(root, text="Add Sensor\nMM/DD/YY", command=add_sensor, bg="white", font="calibri 15 bold")
filter_button = tk.Button(root, text="Filter", command=get_files, bg="white", font="calibri 15 bold")
reset_button = tk.Button(root, text="Reset", command=reset, bg="white", font="calibri 15 bold")
smartrock_type_button = tk.Button(root, text="SmartRock3", command=change_sensor_type, bg="white",
                                  font="calibri 15 bold")

rtd_button.grid(row=1, column=1, sticky=tk.N + tk.E + tk.W + tk.S)
rtd_label.grid(row=1, column=3, sticky=tk.N + tk.E + tk.W + tk.S)
sensor_button.grid(row=3, column=1, sticky=tk.N + tk.E + tk.W + tk.S)
sensor_label.grid(row=3, column=3, sticky=tk.N + tk.E + tk.W + tk.S)
filter_button.grid(row=5, column=1, sticky=tk.N + tk.E + tk.W + tk.S)
reset_button.grid(row=5, column=3, sticky=tk.S + tk.E + tk.W + tk.N)
smartrock_type_button.grid(row=7, column=1, columnspan=3, sticky=tk.S + tk.E + tk.W + tk.N)

root.mainloop()

'''                /
                  O
                 /
    ____________/__
    \          /  /
     \~~~~~~~~~~~/
      \         /
       \       /
        \     /
         \   /
          | |
          | |
          | |
          | |
          | |
          | |
          | |
      ~--/   \ --, 
    /   /     \   \ 
    \_____________/
   
'''